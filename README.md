# A SIMPLE MUSIC SORTING PYTHON SCRIPT

This is a small python script that sorts unsorted mp3 files based on the Artist and the Album.
It Reads the metadata of the mp3 for the artist's and album's name.

#Instructions
Just run the script, select the folder you want to sort, and you're done

#Dependencies
If you're using this script for the first time, run "pip install -r requirements.txt"

If you're running Arch/Manjaro you might need to install tk, to do that type "pacman -S tk"


